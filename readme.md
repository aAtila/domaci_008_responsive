#Domaći zadatak

* Kreirati responsivnu web stranicu sa slikama i video materijalima
* Koristiti tri prelomne tacke (breaking points)
* Na širinama ekrana do 800px navigacija i cela struktura sajta treba da se promene
* Externi CSS fajl obavezan!
* Koristiti normalize.css i css minifier
* Kreirani sajt postaviti na internet